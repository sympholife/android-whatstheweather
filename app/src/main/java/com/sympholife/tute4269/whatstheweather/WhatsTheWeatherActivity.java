package com.sympholife.tute4269.whatstheweather;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class WhatsTheWeatherActivity extends AppCompatActivity {

    EditText weatherInput;
    TextView weatherResult;
    Button getWeatherButton;

    public class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(urls[0]);
                urlConnection = (HttpURLConnection) url.openConnection();

                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);

                int data = reader.read();
                while (data != -1) {
                    char current = (char) data;
                    result += current;
                    data = reader.read();
                }

                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        // Unlike doInBackground, you can modify UI Elements in this method
        // the parameter in this method is the result from the doInBackground
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {

                // encode the result as a JSONObject (it fails if the JSON is malformed)
                JSONObject jsonObject = new JSONObject(result);
                // gets the key 'weather' from the json
                String weatherInfo = jsonObject.getString("weather");
                Log.i("Weather content", weatherInfo);

                // since weatherInfo (the 'weather key') contains an array of other keys and values,
                // create a new JSONArray from the weather info
                JSONArray arr = new JSONArray(weatherInfo);

                // get the 'main' and 'description' keys from the array
                for (int i = 0; i < arr.length(); i++) {
                    // get the necessary parts as a new JSONObject
                    JSONObject jsonPart = arr.getJSONObject(i);

                    Log.i("main", jsonPart.getString("main"));
                    Log.i("description", jsonPart.getString("description"));

                    weatherResult.setText(jsonPart.getString("main") + " : " + jsonPart.getString("description"));
                    weatherResult.setVisibility(View.VISIBLE);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whats_the_weather);

        weatherInput = (EditText) findViewById(R.id.weatherInput);
        weatherResult = (TextView) findViewById(R.id.weatherResult);
        getWeatherButton = (Button) findViewById(R.id.getWeatherButton);

        weatherResult.setVisibility(View.INVISIBLE);
    }

    public void getWeather(View view) {
        String city = weatherInput.getText().toString();
        // create task and execute
        DownloadTask task = new DownloadTask();
        try {
            task.execute("http://api.openweathermap.org/data/2.5/weather?q=" + city.replaceAll(" ", "%20") + "&APPID=8b4dfcbbe7b2fb4d2b176e7460f701f0");
        } catch (Exception e) {
            weatherResult.setText("There aren't any results for the city you entered.");
            weatherResult.setVisibility(View.VISIBLE);
        }

    }

}
